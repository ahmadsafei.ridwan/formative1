package com.company;

import java.util.Arrays;
import java.util.Date;

public class Cleaning extends Employee{
    String[] cleaningGears = {"Mop", "Broom"};

    Cleaning(String name, Date startDate, int salary, String uniform){
        super.setSalary(salary);
        super.name = name;
        super.startDate = startDate;
        super.uniform = uniform;
    }

    @Override
    void getIdentityEmployee() {
        System.out.println("CLEANING EMPLOYEE");
        System.out.println("Nama\t\t\t\t: " + super.name);
        System.out.println("Start Date\t\t\t: " + super.startDate);
        System.out.println("Salary\t\t\t\t: " + super.getSalary());
        System.out.println("Start Working Hour \t: " + startWorkingHour);
        System.out.println("End Working Hour \t: " + endWorkingHour);
        System.out.println("Uniform\t\t\t\t: " + super.uniform);
        System.out.println("Cleaning Gears\t\t: " + Arrays.toString(this.cleaningGears));
        System.out.println("================================\n");
    }
}
