package com.company;

import java.util.Date;
public abstract class Employee extends Bioskop{
    String name;
    Date startDate;
    private int salary;
    static int startWorkingHour = 8;
    static int endWorkingHour = 16;
    String uniform;

    abstract void getIdentityEmployee();

    void setSalary(int salary){
        this.salary = salary;
    }

    int getSalary(){
        return this.salary;
    }
}
