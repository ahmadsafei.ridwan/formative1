package com.company;

public class Bioskop {
    String name;
    String address;
    String openingHours = "09.00 am";

    Bioskop(){
        this.name = "Bioskop 21 Jakarta";
        this.address = "Jl. Jenderal Soedirman Jakarta Pusat";
    }

    void getBioskopData(){
        System.out.println("Nama Bioskop   : " + this.name);
        System.out.println("Alamat Bioskop : " + this.address);
        System.out.println("Jam Operasional: " + this.openingHours);
        System.out.println("================================\n");
    }
}
