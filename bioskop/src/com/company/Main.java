package com.company;
import java.util.Date;
public class Main {

    public static void main(String[] args) {
        Bioskop bioskop = new Bioskop();
        bioskop.getBioskopData();

        Employee cashier = new Cashier("Sudirman",new Date(2011,12,3),4000000,"Putih");
        if(cashier instanceof Cashier){
            Cashier downcastCashier = (Cashier) cashier;
            downcastCashier.setHasilPenjualanTicket(100);
        }
        cashier.getIdentityEmployee();

        Employee security = new Security("Vina",new Date(2020,01,01), 3000000, "Hitam");
        security.getIdentityEmployee();

        Employee cleaning = new Cleaning("Budi", new Date(2018,02,23), 2000000,"Pink");
        cleaning.getIdentityEmployee();

        Movies avengers = new Movies(65000, "Avengers Back to Hometown");
        Movies captainAmerica = new Movies(50000, "Civil War");

        Studio studio1 = new Studio("studio1", avengers);
        studio1.getDisplayStudio();
        Studio studio2 = new Studio("studio2", captainAmerica);
        studio2.getDisplayStudio();


    }
}
