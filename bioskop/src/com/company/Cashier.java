package com.company;

import java.util.Date;

public class Cashier extends Employee{
    int hasilPenjualanTicket;

    Cashier(String name, Date startDate, int salary, String uniform){
        super.setSalary(salary);
        super.name = name;
        super.startDate = startDate;
        super.uniform = uniform;
    }

    void setHasilPenjualanTicket(int hasilPenjualanTicket){
        this.hasilPenjualanTicket = hasilPenjualanTicket;
    }

    @Override
    void getIdentityEmployee() {
        System.out.println("CASHIER EMPLOYEE");
        System.out.println("Nama\t\t\t\t: " + super.name);
        System.out.println("Start Date\t\t\t: " + super.startDate);
        System.out.println("Salary\t\t\t\t: " + super.getSalary());
        System.out.println("Start Working Hour \t: " + startWorkingHour);
        System.out.println("End Working Hour \t: " + endWorkingHour);
        System.out.println("Uniform\t\t\t\t: " + super.uniform);
        System.out.println("Penjualan Tiket\t\t: " + this.hasilPenjualanTicket);
        System.out.println("================================\n");
    }

}
