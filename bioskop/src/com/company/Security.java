package com.company;

import java.util.Arrays;
import java.util.Date;

public class Security extends Employee{
    String[] certification = {"The CompTIA Security+"};
    String[] securityGears = {"Stick", "Armor", "Gun"};

    Security(String name, Date startDate, int salary, String uniform){
        super.setSalary(salary);
        super.name = name;
        super.startDate = startDate;
        super.uniform = uniform;
    }

    @Override
    void getIdentityEmployee() {
        System.out.println("SECURITY EMPLOYEE");
        System.out.println("Nama\t\t\t\t: " + super.name);
        System.out.println("Start Date\t\t\t: " + super.startDate);
        System.out.println("Salary\t\t\t\t: " + super.getSalary());
        System.out.println("Start Working Hour \t: " + startWorkingHour);
        System.out.println("End Working Hour \t: " + endWorkingHour);
        System.out.println("Uniform\t\t\t\t: " + super.uniform);
        System.out.println("Certification\t\t: " + Arrays.toString(this.certification));
        System.out.println("Security Gears\t\t: " + Arrays.toString(this.securityGears));
        System.out.println("================================\n");
    }

}
