package com.company;

public class Studio extends Bioskop{
    String name;
    public int seatsAmount = 100;
    Movies movies;

    Studio(String name, Movies movies){
        this.name = name;
        this.movies = movies;
    }

    void getDisplayStudio(){
        System.out.println("Studio name : " + this.name);
        System.out.println("Seats Amount: " + this.seatsAmount);
        System.out.println("Title Movie : " + movies.title);
        System.out.println("Price Movie : "+ movies.price +"\n");
    }

}
